from django.shortcuts import render, get_object_or_404
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.


def show_todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
        else:
            form = TodoListForm()
        context = {
            "form": form,
        }
        return render(request, "todo_list/create.html", context)
