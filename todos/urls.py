from django.urls import path
from todos.views import show_todo_list, todo_list_detail


urlpatterns = [
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", show_todo_list, name="todo_list_list"),
]
